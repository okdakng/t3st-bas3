/* Просмотр списка Базы Данных */
SHOW DATEBASES;

/* Удаление Базы Данных если уже существует */
DROP DATEBASE IF EXISTS test_base;
/* Созданин самой Базы Данных */
CREATE DATEBASE test_base;

/* Создание таблицы */
Use test_base;
CREATE TABLE `groups` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(200) NOT NULL,
    PRIMARY KEY(`id`)
);
CREATE TABLE `students`(
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(200) NOT NULL,
    `group_id` INT(11) DEFAULT NULL,
    `birthday` DATETIME NOT NULL,
    PRIMARY KEY(id)
);